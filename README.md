> Lint your conventional commits

# @sysadmiral/commitlint-config

Shareable `commitlint` config based on [conventional commits](https://conventionalcommits.org/).
Use with [@commitlint/cli](https://npm.im/@commitlint/cli) and [@commitlint/prompt-cli](https://npm.im/@commitlint/prompt-cli).

## Changes from @commitlint/config-conventional

- remove chore type
- add deploy type